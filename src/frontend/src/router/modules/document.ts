const document: AuthRoute.Route = {
  name: 'document',
  path: '/document',
  component: 'basic',
  children: [
    {
      name: 'document_vue',
      path: '/document/vue',
      component: 'self',
      meta: {
        title: 'Vue文档',
        requiresAuth: true,
        icon: 'mdi:vuejs'
      }
    },
    {
      name: 'document_vite',
      path: '/document/vite',
      component: 'self',
      meta: {
        title: 'Vite文档',
        requiresAuth: true,
        icon: 'simple-icons:vite'
      }
    },
    {
      name: 'document_python',
      path: '/document/python',
      component: 'self',
      meta: {
        title: 'Python文档',
        requiresAuth: true,
        icon: 'simple-icons:python'
      }
    },
    {
      name: 'document_fastapi',
      path: '/document/fastapi',
      component: 'self',
      meta: {
        title: 'FastAPI文档',
        requiresAuth: true,
        icon: 'simple-icons:fastapi'
      }
    },
    {
      name: 'document_project',
      path: '/document/project',
      meta: {
        title: '项目(外链)',
        requiresAuth: true,
        icon: 'mdi:file-link-outline',
        href: 'https://gitee.com/ssshier/ton-dmp/'
      }
    }
  ],
  meta: {
    title: '文档中心',
    icon: 'carbon:document',
    order: 2
  }
};

export default document;
