const database: AuthRoute.Route = {
  name: 'database',
  path: '/database',
  component: 'basic',
  children: [
    {
      name: 'database_tool',
      path: '/database/tool',
      component: 'self',
      meta: {
        title: '工作台',
        requiresAuth: true,
        icon: 'icon-park-outline:workbench'
      }
    }
  ],
  meta: {
    title: '数据库',
    icon: 'mdi:database',
    order: 1
  }
};

export default database;
