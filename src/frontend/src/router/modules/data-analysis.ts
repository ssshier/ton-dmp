const dataAnalysis: AuthRoute.Route = {
  name: 'data-analysis',
  path: '/data-analysis',
  component: 'basic',
  children: [
    {
      name: 'data-analysis_examples',
      path: '/data-analysis/examples',
      component: 'self',
      meta: {
        title: '示例',
        requiresAuth: true,
        icon: 'icon-park-outline:analysis'
      }
    }
  ],
  meta: {
    title: '数据分析',
    icon: 'icon-park-outline:analysis',
    order: 1
  }
};

export default dataAnalysis;
