// import { adapter } from '@/utils';
import { request } from '../../request';

/**
 * 获取DatabaseConnection列表
 */
export async function fetchDatabaseConnectionList() {
  const result = await request.get('/api/v1/db/connection-list');
  return result;
}

/**
 * 获取DatabaseInfomation
 */
export async function fetchDatabaseInfomation(params: any) {
  const result = await request.get('/api/v1/db/information', params);
  return result;
}

/**
 * 执行SQL语句
 */
export async function excuteSqlStatement(data: any) {
  const result = await request.post('/api/v1/db/execute-statement', data);
  return result;
}
