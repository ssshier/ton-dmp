export enum EnumDatabaseType {
  'POSTGRESQL' = 'POSTGRESQL',
  'MYSQL' = 'MYSQL'
}

export enum EnumDatabaseQueryType {
  'schema' = 'SCHEMA',
  'table' = 'TABLE',
  'sequence' = 'SEQUENCE',
  'view' = 'VIEW',
  'function' = 'FUNCTION',
  'trigger' = 'TRIGGER',
  'procedure' = 'PROCEDURE',
  'package' = 'PACKAGE',
  'column' = 'COLUMN'
}

export enum EnumDatabaseQueryTypeIcon {
  'schema' = 'mdi:database',
  'table' = 'mdi:table',
  'sequence' = 'mdi:view-sequential',
  'view' = 'mdi:view-comfy',
  'function' = 'mdi:function',
  'trigger' = 'arcticons:trigger',
  'procedure' = 'ant-design:delivered-procedure-outlined',
  'package' = 'mdi:package',
  'column' = 'mdi:table-column'
}

export enum EnumDatabaseTypeIcon {
  'POSTGRESQL' = 'logos:postgresql',
  'MYSQL' = 'logos:mysql'
}
