export interface DatabaseConnection {
  id: number;
  connection_name: string;
  connection_type: string;
  host: string;
  port: string;
  user: string;
  password: string;
  database?: string;
}
