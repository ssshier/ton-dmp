from datetime import timedelta
from typing import Type

from fastapi import HTTPException
from sqlmodel.ext.asyncio.session import AsyncSession

from app import crud, models, schemas
from app.core.config import settings
from app.core.security import create_access_token, get_password_hash, verify_password
from app.services.base import BaseService


class UserService(BaseService):
    async def auth(self, session: AsyncSession, username: str, password: str):
        user = await self.crud.get_by_username(session, username)
        if not user or not verify_password(password, user.password):
            raise HTTPException(
                status_code=400, detail="Incorrect username or password"
            )
        if not user.is_active:
            raise HTTPException(status_code=400, detail="Inactive user")
        access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
        return {
            "access_token": create_access_token(
                user.id, expires_delta=access_token_expires
            ),
            "token_type": "bearer",
        }

    async def create(self, session: AsyncSession, obj: schemas.UserCreate):
        obj.password = get_password_hash(obj.password)
        return await self.crud.create(session, obj=models.User(**obj.dict()))


user_service = UserService(models.User)
