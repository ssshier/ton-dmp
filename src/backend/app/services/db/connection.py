from typing import Type

from app import crud, models
from app.services.base import BaseService


class ConnectionService(BaseService):
    pass


connection_service = ConnectionService(models.Connection)
