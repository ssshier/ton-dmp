from typing import Optional

import sqlparse
from sqlmodel.ext.asyncio.session import AsyncSession

from app import crud, models
from app.common.enums.db import SQLType
from app.core.db.db_dsn import DBDns
from app.core.db.db_func import get_sql_type
from app.core.db.db_obj import DBObj


class DBPublicService:
    async def get_schemas(self, session: AsyncSession, connection_id: int):
        db_info = await self._get_db_info(session, connection_id)
        return await db_info.get_schemas()

    async def get_schema_tables(
        self, session: AsyncSession, connection_id: int, schema: str
    ):
        db_info = await self._get_db_info(session, connection_id)
        return await db_info.get_schema_tables(schema)

    async def get_schema_views(
        self, session: AsyncSession, connection_id: int, schema: str
    ):
        db_info = await self._get_db_info(session, connection_id)
        return await db_info.get_schema_views(schema)

    async def get_schema_functions(
        self, session: AsyncSession, connection_id: int, schema: str
    ):
        db_info = await self._get_db_info(session, connection_id)
        return await db_info.get_schema_functions(schema)

    async def get_schema_triggers(
        self, session: AsyncSession, connection_id: int, schema: str
    ):
        db_info = await self._get_db_info(session, connection_id)
        return await db_info.get_schema_triggers(schema)

    async def get_table_columns(
        self, session: AsyncSession, connection_id: int, schema: str, table: str
    ):
        db_info = await self._get_db_info(session, connection_id)
        return await db_info.get_table_columns(schema, table)

    async def execute_statement(
        self, session: AsyncSession, connection_id: int, schema: str, statement: str
    ):
        conn: models.Connection = await crud.connection_crud.read(
            session, pk=connection_id
        )
        handler = self._get_db_handler(conn, schema)
        sql_type = get_sql_type(statement)
        if sql_type == SQLType.DQL.value:
            cur = await handler.execute_dql(statement)
            return handler.fetchall(cur)
        elif sql_type == SQLType.DML.value:
            cur = await handler.execute_dml(statement)
            return handler.returns_rows(cur)
        raise ValueError("类型不支持")

    def _get_db_handler(self, conn: models.Connection, schema: Optional[str] = None):
        url = DBDns.async_dns(
            connection_type=conn.connection_type,
            host=conn.host,
            port=conn.port,
            user=conn.user,
            password=conn.password,
            database=conn.database or schema,
        )
        return DBObj(db_type=conn.connection_type).db_handler()(url=url)

    async def _get_db_info(self, session: AsyncSession, connection_id: int):
        conn: models.Connection = await crud.connection_crud.read(
            session, pk=connection_id
        )
        handler = self._get_db_handler(conn)
        db_obj = DBObj(db_type=conn.connection_type)
        db_statements = db_obj.db_statements()
        return db_obj.db_info()(handler, db_statements)


db_public_service = DBPublicService()
