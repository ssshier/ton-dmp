from typing import Type

from app import crud, models
from app.services.base import BaseService


class ExcService(BaseService):
    pass


exc_service = ExcService(models.Exc)
