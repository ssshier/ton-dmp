from app.tasks.task_producer import TaskProducer


class TaskService:
    def __init__(self):
        self.task_producer = TaskProducer()

    async def create(self, func: str, *args):
        await self.task_producer.enqueue_job(func, *args)
