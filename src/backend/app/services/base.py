from typing import Generic, List, Optional, Type, TypeVar

from sqlmodel import SQLModel
from sqlmodel.ext.asyncio.session import AsyncSession

from app import crud
from app.models.base import BaseSQLModel

ModelType = TypeVar("ModelType", bound=BaseSQLModel)
CreateSchema = TypeVar("CreateSchema", bound=SQLModel)
UpdateSchema = TypeVar("UpdateSchema", bound=SQLModel)


class BaseService(Generic[ModelType, CreateSchema, UpdateSchema]):
    def __init__(self, model: Type[ModelType]):
        self.model = model
        self.crud = getattr(crud, model.__name__.lower() + "_crud")

    async def create(self, session: AsyncSession, obj: CreateSchema) -> ModelType:
        return await self.crud.create(session, obj=self.model(**obj.dict()))

    async def read(self, session: AsyncSession, pk: int) -> Optional[ModelType]:
        return await self.crud.read(session, pk=pk)

    async def update(
        self, session: AsyncSession, pk: int, obj: UpdateSchema
    ) -> Optional[ModelType]:
        return await self.crud.update(session, pk=pk, obj=obj)

    async def delete(self, session: AsyncSession, pk: int) -> Optional[ModelType]:
        return await self.crud.delete(session, pk=pk)

    async def list(self, session: AsyncSession) -> List[ModelType]:
        return await self.crud.list(session)
