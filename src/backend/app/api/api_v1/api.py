from fastapi import APIRouter, Depends

from app.api import deps
from app.api.api_v1.endpoints import (
    connection_router,
    db_public_router,
    exc_router,
    login_router,
    notify_router,
    task_router,
    user_router,
)

api_router = APIRouter()
api_router.include_router(login_router, tags=["login"])
api_router.include_router(
    user_router,
    prefix="/user",
    tags=["user"],
    dependencies=[Depends(deps.get_current_active_superuser)],
)
api_router.include_router(
    exc_router,
    prefix="/com",
    tags=["com"],
    dependencies=[Depends(deps.get_current_active_user)],
)
api_router.include_router(
    task_router,
    prefix="/task",
    tags=["task"],
    dependencies=[Depends(deps.get_current_active_user)],
)
api_router.include_router(connection_router, prefix="/db", tags=["db"])
api_router.include_router(db_public_router, prefix="/db", tags=["db"])
api_router.include_router(notify_router, prefix="/ws", tags=["ws"])
