from typing import Any

from fastapi import APIRouter, Body, Depends
from sqlmodel.ext.asyncio.session import AsyncSession

from app import services
from app.api.resp import CustomJSONResponse
from app.common.enums.db import DBQueryType
from app.db.session import get_session

router = APIRouter()


@router.get("/information", response_class=CustomJSONResponse)
async def get_information(
    query_type: str,
    connection_id: int,
    schema: str = None,
    table: str = None,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Retrieve information.
    """
    if query_type == DBQueryType.SCHEMA.value:
        return await services.db_public_service.get_schemas(session, connection_id)
    elif query_type == DBQueryType.TABLE.value:
        return await services.db_public_service.get_schema_tables(
            session, connection_id, schema
        )
    elif query_type == DBQueryType.VIEW.value:
        return await services.db_public_service.get_schema_views(
            session, connection_id, schema
        )
    elif query_type == DBQueryType.FUNCTION.value:
        return await services.db_public_service.get_schema_functions(
            session, connection_id, schema
        )
    elif query_type == DBQueryType.TRIGGER.value:
        return await services.db_public_service.get_schema_triggers(
            session, connection_id, schema
        )
    elif query_type == DBQueryType.COLUMN.value:
        return await services.db_public_service.get_table_columns(
            session, connection_id, schema, table
        )
    raise ValueError("类型不支持")


@router.post("/execute-statement", response_class=CustomJSONResponse)
async def execute_statement(
    *,
    statement: str = Body(...),
    connection_id: int = Body(...),
    schema: str = Body(...),
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Execute statement.
    """
    return await services.db_public_service.execute_statement(
        session, connection_id, schema, statement
    )
