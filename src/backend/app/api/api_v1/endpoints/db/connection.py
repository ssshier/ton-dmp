from typing import Any

from fastapi import APIRouter, Depends
from sqlmodel.ext.asyncio.session import AsyncSession

from app import schemas, services
from app.api.resp import CustomJSONResponse
from app.db.session import get_session

router = APIRouter()


@router.post("/connection", response_class=CustomJSONResponse)
async def create_connection(
    *,
    obj: schemas.ConnectionCreate,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Create new connection.
    """
    return await services.connection_service.create(session, obj)


@router.get("/connection/{pk}", response_class=CustomJSONResponse)
async def read_connection(
    pk: int,
    *,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Retrieve connection.
    """
    return await services.connection_service.read(session, pk)


@router.put("/connection/{pk}", response_class=CustomJSONResponse)
async def update_connection(
    pk: int,
    *,
    obj: schemas.ConnectionUpdate,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Update a connection.
    """
    return await services.connection_service.update(session, pk, obj)


@router.delete("/connection/{pk}", response_class=CustomJSONResponse)
async def delete_connection(
    pk: int,
    *,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Delete a connection.
    """
    return await services.connection_service.delete(session, pk)


@router.get("/connection-list", response_class=CustomJSONResponse)
async def list_connection(session: AsyncSession = Depends(get_session)) -> Any:
    """
    List connection.
    """
    return await services.connection_service.list(session)
