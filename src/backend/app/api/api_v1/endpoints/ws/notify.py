import asyncio
from typing import Any

from fastapi import APIRouter, Body, WebSocket, WebSocketDisconnect
from fastapi.responses import HTMLResponse
from loguru import logger

from app.api.resp import CustomJSONResponse
from app.core.message.pubsub import PubSub
from app.core.message.ws_manager import ws_manager

router = APIRouter()


html = """
<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
    </head>
    <body>
        <h1>WebSocket Chat</h1>
        <h2>Your ID: <span id="ws-id"></span></h2>
        <form action="" onsubmit="sendMessage(event)">
            <input type="text" id="messageText" autocomplete="off"/>
            <button>Send</button>
        </form>
        <ul id='messages'>
        </ul>
        <script>
            var client_id = Date.now()
            document.querySelector("#ws-id").textContent = client_id;
            var ws = new WebSocket(`ws://localhost:9000/api/v1/ws/notify/${client_id}`);
            ws.onmessage = function(event) {
                var messages = document.getElementById('messages')
                var message = document.createElement('li')
                var content = document.createTextNode(event.data)
                message.appendChild(content)
                messages.appendChild(message)
            };
            function sendMessage(event) {
                var input = document.getElementById("messageText")
                ws.send(input.value)
                input.value = ''
                event.preventDefault()
            }
        </script>
    </body>
</html>
"""


@router.get("/notify/index")
async def get():
    return HTMLResponse(html)


@router.post("/notify/pub", response_class=CustomJSONResponse)
async def pub_notify(message: str = Body(...)):
    pubsub = PubSub()
    await pubsub.publish(message)
    return "Pub success"


@router.websocket("/notify/{client_id}")
async def ws_notify(websocket: WebSocket, client_id: str) -> Any:
    """
    Websocket notify.
    """
    await ws_manager.connect(websocket, client_id)
    while True:
        if not ws_manager.heartbeat(client_id):
            break
        await asyncio.sleep(1)
