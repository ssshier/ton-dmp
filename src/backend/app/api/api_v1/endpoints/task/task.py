from typing import Any

from fastapi import APIRouter

from app.api.resp import CustomJSONResponse
from app.services.task.task import TaskService

router = APIRouter()


@router.post("/task", response_class=CustomJSONResponse)
async def create_task(
    func: str,
) -> Any:
    """
    Create new task.
    """
    return await TaskService().create(func)
