from typing import Any

from fastapi import APIRouter, Depends
from sqlmodel.ext.asyncio.session import AsyncSession

from app import schemas, services
from app.api.resp import CustomJSONResponse
from app.db.session import get_session

router = APIRouter()


@router.post("/user", response_class=CustomJSONResponse)
async def create_user(
    *,
    obj: schemas.UserCreate,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Create new user.
    """
    return await services.user_service.create(session, obj)


@router.get("/user/{pk}", response_class=CustomJSONResponse)
async def read_user(
    pk: int,
    *,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Retrieve user.
    """
    return await services.user_service.read(session, pk)


@router.put("/user/{pk}", response_class=CustomJSONResponse)
async def update_user(
    pk: int,
    *,
    obj: schemas.UserUpdate,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Update a user.
    """
    return await services.user_service.update(session, pk, obj)


@router.delete("/user/{pk}", response_class=CustomJSONResponse)
async def delete_user(
    pk: int,
    *,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Delete a user.
    """
    return await services.user_service.delete(session, pk)


@router.get("/user-list", response_class=CustomJSONResponse)
async def list_user(session: AsyncSession = Depends(get_session)) -> Any:
    """
    List user.
    """
    return await services.user_service.list(session)
