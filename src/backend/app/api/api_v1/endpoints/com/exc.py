from typing import Any

from fastapi import APIRouter, Depends
from sqlmodel.ext.asyncio.session import AsyncSession

from app import schemas, services
from app.api.resp import CustomJSONResponse
from app.db.session import get_session

router = APIRouter()


@router.post("/exc", response_class=CustomJSONResponse)
async def create_exc(
    *,
    obj: schemas.ExcCreate,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Create new exc.
    """
    return await services.exc_service.create(session, obj)


@router.get("/exc/{pk}", response_class=CustomJSONResponse)
async def read_exc(
    pk: int,
    *,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Retrieve exc.
    """
    return await services.exc_service.read(session, pk)


@router.put("/exc/{pk}", response_class=CustomJSONResponse)
async def update_exc(
    pk: int,
    *,
    obj: schemas.ExcUpdate,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Update a exc.
    """
    return await services.exc_service.update(session, pk, obj)


@router.delete("/exc/{pk}", response_class=CustomJSONResponse)
async def delete_exc(
    pk: int,
    *,
    session: AsyncSession = Depends(get_session),
) -> Any:
    """
    Delete a exc.
    """
    return await services.exc_service.delete(session, pk)


@router.get("/exc-list", response_class=CustomJSONResponse)
async def list_exc(session: AsyncSession = Depends(get_session)) -> Any:
    """
    List exc.
    """
    return await services.exc_service.list(session)
