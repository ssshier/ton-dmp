import json
import typing
from typing import Generic, TypeVar

from pydantic.generics import GenericModel
from starlette.responses import Response

T = TypeVar("T")


class ResponseCode:
    success: int = 200


class ResponseModel(GenericModel, Generic[T]):
    code: int
    data: T
    message: str = ""


class SuccessResponseModel(ResponseModel):
    code: int = ResponseCode.success


class CustomJSONResponse(Response):
    media_type = "application/json"

    def render(self, content: typing.Any) -> bytes:
        result = {"code": ResponseCode.success, "data": content, "message": "请求成功"}
        return json.dumps(
            result,
            ensure_ascii=False,
            allow_nan=False,
            indent=None,
            separators=(",", ":"),
        ).encode("utf-8")
