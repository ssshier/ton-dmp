def singleton(cls):
    _connection = {}

    def _singleton(*args, **kwargs):
        if cls not in _connection:
            _connection[cls] = cls(*args, **kwargs)
        return _connection[cls]

    return _singleton
