from enum import Enum


class DBType(Enum):

    MYSQL = "MYSQL"
    POSTGRESQL = "POSTGRESQL"


class SQLType(Enum):

    DQL = "DQL"
    DML = "DML"
    DDL = "DDL"
    DCL = "DCL"


class DBQueryType(Enum):
    SCHEMA = "SCHEMA"
    TABLE = "TABLE"
    VIEW = "VIEW"
    FUNCTION = "FUNCTION"
    TRIGGER = "TRIGGER"
    COLUMN = "COLUMN"
