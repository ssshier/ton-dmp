from typing import Optional

from sqlmodel import Field, SQLModel

from app.models.base import BaseSQLModel


class ConnectionBase(SQLModel):
    connection_name: str = Field(nullable=False, max_length=32)
    connection_type: str = Field(nullable=False, max_length=32)
    host: str = Field(nullable=False, max_length=32)
    port: str = Field(nullable=False, max_length=32)
    user: str = Field(nullable=True, max_length=32)
    password: str = Field(nullable=True, max_length=32)
    database: str = Field(nullable=True, max_length=32)


class Connection(BaseSQLModel, ConnectionBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)


class ConnectionCreate(ConnectionBase):
    pass


class ConnectionRead(ConnectionBase):
    id: int


class ConnectionUpdate(ConnectionBase):
    pass
