from typing import Optional

from pydantic import EmailStr, validator
from sqlmodel import Field, SQLModel

from app.core.security import get_password_hash
from app.models.base import BaseSQLModel


class UserBase(SQLModel):
    username: str = Field(
        nullable=False, max_length=32, sa_column_kwargs={"unique": True}
    )
    password: str = Field(nullable=False, max_length=128)

    avatar_url: Optional[str] = Field(max_length=128)
    phone: str = Field(nullable=False, max_length=32, sa_column_kwargs={"unique": True})
    email: EmailStr = Field(nullable=False, sa_column_kwargs={"unique": True})

    is_superuser: bool = Field(nullable=False, default=False)
    is_active: bool = Field(nullable=False, default=True)


class User(BaseSQLModel, UserBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)


class UserCreate(UserBase):
    pass


class UserRead(UserBase):
    id: int


class UserUpdate(UserBase):
    pass
