from datetime import datetime

from sqlmodel import Field, SQLModel


class BaseSQLModel(SQLModel):

    created_at: datetime = Field(default=datetime.utcnow(), nullable=False)
    updated_at: datetime = Field(default_factory=datetime.utcnow, nullable=False)

    created_by: str = Field(default="SYSTEM", nullable=False)
    updated_by: str = Field(default="SYSTEM", nullable=False)

    is_deleted: int = Field(default=0)
