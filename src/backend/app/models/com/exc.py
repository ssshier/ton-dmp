from typing import Optional

from sqlmodel import Field, SQLModel

from app.models.base import BaseSQLModel


class ExcBase(SQLModel):
    app_name: str = Field(nullable=False, max_length=32)
    app_code: str = Field(nullable=False, max_length=32)
    module_name: str = Field(nullable=False, max_length=32)
    module_code: str = Field(nullable=False, max_length=32)

    name: str = Field(nullable=False, max_length=32)
    code: str = Field(nullable=False, max_length=32)
    message: str = Field(nullable=False, max_length=128)


class Exc(BaseSQLModel, ExcBase, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)


class ExcCreate(ExcBase):
    pass


class ExcRead(ExcBase):
    id: int


class ExcUpdate(ExcBase):
    pass
