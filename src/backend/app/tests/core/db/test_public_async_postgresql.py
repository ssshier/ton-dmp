import asyncio
import unittest

from objprint import op

from app.common.decorators.aio import async_test
from app.core.db.db_obj import DBObj


class TestClass(unittest.TestCase):
    def setUp(self):
        self.loop = asyncio.get_event_loop()
        self.db_type = "POSTGRESQL"
        self.url = "postgresql+asyncpg://postgres:postgres@127.0.0.1:5432/postgres"
        self.schema = "public"
        self.table = "connection"

        self.db_obj = DBObj(self.db_type)
        self.handler = self.db_obj.db_handler()(url=self.url)
        self.db_statements = self.db_obj.db_statements()
        self.db_info = self.db_obj.db_info()(self.handler, self.db_statements)

    @async_test
    async def test_get_schemas(self):
        result = await self.db_info.get_schemas()
        op(result)
        assert True

    @async_test
    async def test_get_schema_tables(self):
        result = await self.db_info.get_schema_tables(self.schema)
        op(result)
        assert True

    @async_test
    async def test_get_schema_views(self):
        result = await self.db_info.get_schema_views(self.schema)
        op(result)
        assert True

    @async_test
    async def test_schema_functions(self):
        result = await self.db_info.get_schema_functions(self.schema)
        op(result)
        assert True

    @async_test
    async def test_schema_triggers(self):
        result = await self.db_info.get_schema_triggers(self.schema)
        op(result)
        assert True

    @async_test
    async def test_table_columns(self):
        result = await self.db_info.get_table_columns(self.schema, self.table)
        op(result)
        assert True


if __name__ == "__main__":
    unittest.main()
