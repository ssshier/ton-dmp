import unittest

from objprint import op

from app.core.db.handler.postgresql_handler import PostgresqlHandler
from app.core.db.information.postgresql_info import PostgresqlInfo


class TestClass(unittest.TestCase):
    def setUp(self):
        self.handler = PostgresqlHandler(
            url="postgresql://postgres:postgres@127.0.0.1:5432/postgres"
        )
        self.schema = "public"
        self.table = "connection"

    def test_get_schemas(self):
        obj = PostgresqlInfo(self.handler)
        result = obj.get_schemas()
        op(result)
        assert True

    def test_get_schema_tables(self):
        obj = PostgresqlInfo(self.handler)
        result = obj.get_schema_tables(self.schema)
        op(result)
        assert True

    def test_get_schema_views(self):
        obj = PostgresqlInfo(self.handler)
        result = obj.get_schema_views(self.schema)
        op(result)
        assert True

    def test_schema_functions(self):
        obj = PostgresqlInfo(self.handler)
        result = obj.get_schema_functions(self.schema)
        op(result)
        assert True

    def test_schema_triggers(self):
        obj = PostgresqlInfo(self.handler)
        result = obj.get_schema_triggers(self.schema)
        op(result)
        assert True

    def test_table_columns(self):
        obj = PostgresqlInfo(self.handler)
        result = obj.get_table_columns(self.schema, self.table)
        op(result)
        assert True


if __name__ == "__main__":
    unittest.main()
