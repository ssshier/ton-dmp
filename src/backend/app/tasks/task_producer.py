from arq.connections import ArqRedis

from app.db.redis import Database


class TaskProducer:
    def __init__(self):
        self.arq_redis: ArqRedis = Database.arq_redis

    async def enqueue_job(self, func, *args):
        await self.arq_redis.enqueue_job(func, args)
