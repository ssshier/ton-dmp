import asyncio

from aiohttp import ClientSession
from arq import cron, worker
from arq.connections import RedisSettings
from loguru import logger

from app.core.config import settings


async def spider(ctx, url):
    session: ClientSession = ctx["session"]
    async with session.get(url) as response:
        content = await response.text()
        print(f"{url}: {content:.80}...")
    return len(content)


async def echo(ctx, st: str):
    await asyncio.sleep(1)
    print(st)


async def run_second(ctx):
    logger.info("start run second task...")


async def startup(ctx):
    ctx["session"] = ClientSession()


async def shutdown(ctx):
    await ctx["session"].close()


# WorkerSettings defines the settings to use when creating the work,
# it's used by the arq cli.
# For a list of available settings, see https://arq-docs.helpmanual.io/#arq.worker.Worker
class WorkerSettings:
    redis_settings = RedisSettings(
        host=settings.REDIS_HOST, port=settings.REDIS_PORT, password=settings.REDIS_PASS
    )
    functions = [spider, echo]
    cron_jobs = [cron(run_second, second=23)]
    on_startup = startup
    on_shutdown = shutdown


async def arq_worker():
    work = worker.Worker(
        functions=WorkerSettings.functions,
        redis_settings=WorkerSettings.redis_settings,
        cron_jobs=WorkerSettings.cron_jobs,
        on_startup=WorkerSettings.on_startup,
        on_shutdown=WorkerSettings.on_shutdown,
    )
    await work.async_run()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(arq_worker())
