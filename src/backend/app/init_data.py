import asyncio
import logging

from sqlmodel.ext.asyncio.session import AsyncSession

from app.db.init_db import init_user
from app.db.session import engine

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


async def init_data() -> None:
    async with AsyncSession(engine) as session:
        await init_user(session)


async def main() -> None:
    logger.info("Creating initial data")
    await init_data()
    logger.info("Initial data created")


if __name__ == "__main__":
    asyncio.run(main())
