from pydantic import AnyUrl, PostgresDsn

from app.common.enums.db import DBType


class MysqlDsn(AnyUrl):
    allowed_schemes = {
        "mysql+pymysql",
        "mysql+aiomysql",
    }
    user_required = True


class DBDns:
    @staticmethod
    def async_dns(connection_type, host, port, user, password, database):
        if connection_type == DBType.POSTGRESQL.value:
            return PostgresDsn.build(
                scheme="postgresql+asyncpg",
                user=user,
                password=password,
                host=host,
                port=port,
                path=f"/{database or ''}",
            )
        elif connection_type == DBType.MYSQL.value:
            return MysqlDsn.build(
                scheme="mysql+aiomysql",
                user=user,
                password=password,
                host=host,
                port=port,
                path=f"/{database or ''}",
            )
        raise ValueError("类型不支持")
