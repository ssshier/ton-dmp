from sqlalchemy.orm import Session

from app.core.db.handler.db_handler import DBHandler


class PostgresqlHandler(DBHandler):
    def execute_dql(self, statement):
        with Session(self.engine) as session:
            result = session.execute(statement)
            return result

    def execute_dml(self, statement):
        with Session(self.engine) as session:
            result = session.execute(statement)
            session.commit()
            return result

    def execute_ddl(self, statement):
        with Session(self.engine) as session:
            result = session.execute(statement)
            session.commit()
            return result

    def execute_dcl(self, statement):
        with Session(self.engine) as session:
            result = session.execute(statement)
            session.commit()
            return result

    def fetchall(self, cur):
        data = []
        for row in cur:
            data.append(row)
        return data
