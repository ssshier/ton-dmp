from sqlalchemy import create_engine
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine


class DBHandler:
    def __init__(self, url, aio=True):
        if aio:
            self.engine = create_async_engine(url, echo=True, future=True)
        else:
            self.engine = create_engine(url, echo=True, future=True)

    async def execute_dql(self, statement):
        async with AsyncSession(self.engine) as session:
            result = await session.execute(statement)
            return result

    async def execute_dml(self, statement):
        async with AsyncSession(self.engine) as session:
            result = await session.execute(statement)
            await session.commit()
            return result

    async def execute_ddl(self, statement):
        async with AsyncSession(self.engine) as session:
            result = await session.execute(statement)
            await session.commit()
            return result

    async def execute_dcl(self, statement):
        async with AsyncSession(self.engine) as session:
            result = await session.execute(statement)
            await session.commit()
            return result

    def fetchall(self, cur):
        data = []
        for row in cur:
            data.append(row)
        return data

    def returns_rows(self, cur):
        return [{"affect_rows": cur.rowcount}]
