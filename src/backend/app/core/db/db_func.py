from app.core.db.sql_lexer import SQLLexer


def get_sql_type(sql: str):
    lexer = SQLLexer()
    results = lexer.tokenize(sql)
    for result in results:
        return result.type
    raise ValueError("类型不支持")
