from app.core.db.handler.postgresql_handler import PostgresqlHandler
from app.core.db.statements import postgresql_statements as statements


class PostgresqlInfo:
    def __init__(self, handler: PostgresqlHandler):
        self.handler = handler

    def get_schemas(self):
        statement = statements.get_schemas
        cur = self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    def get_schema_tables(self, schema):
        statement = statements.get_schema_tables.format(schema=schema)
        cur = self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    def get_schema_views(self, schema):
        statement = statements.get_schema_views.format(schema=schema)
        cur = self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    def get_schema_functions(self, schema):
        statement = statements.get_schema_functions.format(schema=schema)
        cur = self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    def get_schema_triggers(self, schema):
        statement = statements.get_schema_triggers.format(schema=schema)
        cur = self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    def get_table_columns(self, schema, table):
        statement = statements.get_table_columns.format(schema=schema, table=table)
        cur = self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)
