class DBInfo:
    def __init__(self, handler, statements=None):
        self.handler = handler
        self.statements = statements

    async def get_schemas(self):
        statement = self.statements.get_schemas
        cur = await self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    async def get_schema_tables(self, schema):
        statement = self.statements.get_schema_tables.format(schema=schema)
        cur = await self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    async def get_schema_views(self, schema):
        statement = self.statements.get_schema_views.format(schema=schema)
        cur = await self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    async def get_schema_functions(self, schema):
        statement = self.statements.get_schema_functions.format(schema=schema)
        cur = await self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    async def get_schema_triggers(self, schema):
        statement = self.statements.get_schema_triggers.format(schema=schema)
        cur = await self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)

    async def get_table_columns(self, schema, table):
        statement = self.statements.get_table_columns.format(schema=schema, table=table)
        cur = await self.handler.execute_dql(statement)
        return self.handler.fetchall(cur)
