from app.core.db.handler.async_mysql_handler import AsyncMysqlHandler
from app.core.db.handler.async_postgresql_handler import AsyncPostgresqlHandler
from app.core.db.information.async_mysql_info import AsyncMysqlInfo
from app.core.db.information.async_postgresql_info import AsyncPostgresqlInfo
from app.core.db.statements import mysql_statements, postgresql_statements


class DBObj:
    def __init__(self, db_type: str, aio=True):
        self.db_type = db_type
        self.aio = aio
        self.obj = {
            "POSTGRESQL": {
                "HANDLER": AsyncPostgresqlHandler,
                "INFO": AsyncPostgresqlInfo,
                "STATEMENTS": postgresql_statements,
            },
            "MYSQL": {
                "HANDLER": AsyncMysqlHandler,
                "INFO": AsyncMysqlInfo,
                "STATEMENTS": mysql_statements,
            },
        }

    def db_handler(self):
        return self.obj[self.db_type]["HANDLER"]

    def db_info(self):
        return self.obj[self.db_type]["INFO"]

    def db_statements(self):
        return self.obj[self.db_type]["STATEMENTS"]
