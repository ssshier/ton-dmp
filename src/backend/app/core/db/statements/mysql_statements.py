get_schemas = """
select schema_name as name from information_schema.schemata;
"""

get_schema_tables = """
select table_name as name from information_schema.tables where table_schema='{schema}' and table_type='BASE TABLE';
"""

get_schema_views = """
select table_name as name from information_schema.views where table_schema='{schema}';
"""

get_schema_functions = """
select routine_name as name from information_schema.routines where routine_schema='{schema}' and routine_type='FUNCTION';
"""

get_schema_triggers = """
select * from information_schema.triggers where trigger_schema='{schema}';
"""

get_table_columns = """
select column_name as name from information_schema.columns where table_schema='{schema}' and table_name='{table}';
"""
