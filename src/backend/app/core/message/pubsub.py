import asyncio

from aioredis import Redis
from aioredis.abc import AbcChannel
from aioredis.pubsub import Receiver
from loguru import logger

from app.common.decorators.singleton import singleton
from app.db.redis import Database

from .ws_manager import WSManager, ws_manager


@singleton
class PubSub:
    def __init__(self):
        self.redis: Redis = Database.pubsub_redis
        self.ws_manager: WSManager = ws_manager
        self.loop = asyncio.get_running_loop()
        self.rcv = Receiver(loop=self.loop)

    async def reader(self):
        logger.info("start pubsub reader...")
        async for channel, message in self.rcv.iter():
            if isinstance(channel, AbcChannel):
                await self.ws_manager.broadcast(message.decode("utf-8"))
            await asyncio.sleep(1)

    async def open_ws_sender(self):
        logger.info("start websocket sender...")
        asyncio.ensure_future(self.reader())

    async def subscribe(self, ch="channel:public"):
        logger.info("start pubsub subscribe...")
        await self.redis.subscribe(self.rcv.channel(ch))

    async def publish(self, obj: str, ch="channel:public"):
        logger.info(f"publish: {obj}")
        await self.redis.publish(ch, obj)

    async def stop(self):
        self.rcv.stop()
