from typing import Dict, List

from fastapi import WebSocket
from starlette.websockets import WebSocketState

from app.common.decorators.singleton import singleton


@singleton
class WSManager:
    def __init__(self):
        self.active_connections: Dict[str, WebSocket] = {}

    async def connect(self, websocket: WebSocket, client_id: str):
        await websocket.accept()
        self.active_connections[client_id] = websocket

    def disconnect(self, client_id: str):
        self.active_connections.pop(client_id)

    async def disconnect_all(self):
        for connection in self.active_connections.values():
            await connection.close()

    def heartbeat(self, client_id: str):
        if client_id in self.active_connections:
            websocket = self.active_connections[client_id]
            if (
                websocket.application_state == WebSocketState.CONNECTED
                and websocket.client_state == WebSocketState.CONNECTED
            ):
                return True
        return False

    async def send_personal_message(self, message: str, websocket: WebSocket):
        await websocket.send_text(message)

    async def broadcast(self, message: str):
        for connection in self.active_connections.values():
            await connection.send_text(message)


ws_manager = WSManager()
