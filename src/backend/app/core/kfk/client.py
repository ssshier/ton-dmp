from aiokafka import AIOKafkaConsumer
from aiokafka import AIOKafkaProducer

from loguru import logger

from app.core.config import settings


class AIOKafkaClient:

    def __init__(self):
        self.producer = AIOKafkaProducer(
            bootstrap_servers=settings.KAFKA_BOOTSTRAP_SERVERS
        )
        self.consumer = AIOKafkaConsumer(
            settings.KAFKA_DEFAULT_TOPIC,
            bootstrap_servers=settings.KAFKA_BOOTSTRAP_SERVERS,
            group_id=settings.KAFKA_DEFAULT_GROUP
        )

    async def produce(self):
        await self.producer.start()
        try:
            # Produce message
            await self.producer.send_and_wait("my_topic", b"Super message")
        finally:
            # Wait for all pending messages to be delivered or expire.
            await self.producer.stop()

    async def consume(self):
        await self.consumer.start()
        try:
            # Consume messages
            async for msg in self.consumer:
                info = f"consumed: {msg.topic} {msg.partition} {msg.offset} {msg.key} {msg.value} {msg.timestamp}"
                logger.info(info)
        finally:
            # Will leave consumer group; perform autocommit if enabled.
            await self.consumer.stop()
