import asyncio
from typing import Optional

import aio_pika
import aio_pika.abc

from loguru import logger

from app.core.config import settings


class PikaClient:

    def __init__(self):
        self.loop = asyncio.get_running_loop()
        self.connection: Optional[aio_pika.RobustConnection] = None

    async def publish(self):
        routing_key = settings.AMQP_DEFAULT_ROUTING_KEY
        channel: aio_pika.abc.AbstractChannel = await self.connection.channel()
        await channel.default_exchange.publish(
            aio_pika.Message(
                body='Hello {}'.format(routing_key).encode()
            ),
            routing_key=routing_key
        )

    async def consume(self):
        queue_name = settings.AMQP_DEFAULT_QUEUE
        # Creating channel
        channel: aio_pika.abc.AbstractChannel = await self.connection.channel()
        # Declaring queue
        queue: aio_pika.abc.AbstractQueue = await channel.declare_queue(
            queue_name,
            auto_delete=True
        )

        async with queue.iterator() as queue_iter:
            # Cancel consuming after __aexit__
            async for message in queue_iter:
                async with message.process():
                    logger.info(message.body)
                    if queue.name in message.body.decode():
                        break

    async def connect(self):
        self.connection = await aio_pika.connect_robust(
            settings.AMQP_SERVER_URL, loop=self.loop
        )

    async def disconnect(self):
        await self.connection.close()
