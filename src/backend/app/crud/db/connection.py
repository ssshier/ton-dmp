from app.crud.base import BaseCRUD
from app.models.db.connection import Connection


class ConnectionCRUD(BaseCRUD):

    pass


connection_crud = ConnectionCRUD(Connection)
