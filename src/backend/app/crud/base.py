from typing import Generic, List, Optional, Type, TypeVar, Union

from sqlmodel import SQLModel, select
from sqlmodel.ext.asyncio.session import AsyncSession

from app.models.base import BaseSQLModel

ModelType = TypeVar("ModelType", bound=BaseSQLModel)
CreateSchema = TypeVar("CreateSchema", bound=SQLModel)
UpdateSchema = TypeVar("UpdateSchema", bound=SQLModel)


class BaseCRUD(Generic[ModelType, CreateSchema, UpdateSchema]):
    def __init__(self, model: Type[ModelType]):
        self.model = model

    async def read(
        self, session: AsyncSession, *, pk: Union[int, str]
    ) -> Optional[ModelType]:
        return await session.get(self.model, pk)

    async def create(self, session: AsyncSession, *, obj: CreateSchema) -> ModelType:
        db_obj: ModelType = self.model(**obj.dict())
        session.add(db_obj)
        await session.commit()
        await session.refresh(db_obj)
        return db_obj

    async def update(
        self, session: AsyncSession, *, pk: Union[int, str], obj: UpdateSchema
    ) -> Optional[ModelType]:
        db_obj = await session.get(self.model, pk)
        data = obj.dict(exclude_unset=True)
        for key, value in data.items():
            setattr(db_obj, key, value)
        session.add(db_obj)
        await session.commit()
        await session.refresh(db_obj)
        return db_obj

    async def delete(self, session: AsyncSession, *, pk: int) -> Optional[ModelType]:
        obj = await session.get(self.model, pk)
        await session.delete(obj)
        await session.commit()
        return obj

    async def list(self, session: AsyncSession) -> List[ModelType]:
        statement = select(self.model)
        result = await session.exec(statement)
        return result.all()
