from typing import Generic, List, Optional, Type, TypeVar, Union

from sqlmodel import Session, SQLModel, select

from app.models.base import BaseSQLModel

ModelType = TypeVar("ModelType", bound=BaseSQLModel)
CreateSchema = TypeVar("CreateSchema", bound=SQLModel)
UpdateSchema = TypeVar("UpdateSchema", bound=SQLModel)


class BaseCRUD(Generic[ModelType]):
    def __init__(self, model: Type[ModelType]):
        self.model = model

    def read(self, session: Session, *, pk: Union[int, str]) -> Optional[ModelType]:
        return session.get(self.model, pk)

    def create(self, session: Session, *, obj: CreateSchema) -> ModelType:
        obj = self.model(**obj.dict())
        session.add(obj)
        session.commit()
        session.refresh(obj)
        return obj

    def update(
        self, session: Session, *, pk: Union[int, str], obj: UpdateSchema
    ) -> ModelType:
        db_obj = session.get(self.model, pk)
        data = obj.dict(exclude_unset=True)
        for key, value in data.items():
            setattr(db_obj, key, value)
        session.add(db_obj)
        session.commit()
        session.refresh(db_obj)
        return db_obj

    def delete(self, session: Session, *, pk: int) -> ModelType:
        obj = session.get(self.model, pk)
        session.delete(obj)
        session.commit()
        return obj

    def list(self, session: Session) -> List[ModelType]:
        statement = select(self.model)
        return session.exec(statement).all()
