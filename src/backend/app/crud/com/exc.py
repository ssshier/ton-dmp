from app.crud.base import BaseCRUD
from app.models.com.exc import Exc


class ExcCRUD(BaseCRUD):

    pass


exc_crud = ExcCRUD(Exc)
