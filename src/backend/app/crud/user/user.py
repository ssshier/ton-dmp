from typing import Optional, Union

from sqlmodel import select
from sqlmodel.ext.asyncio.session import AsyncSession

from app.crud.base import BaseCRUD
from app.models.user.user import User


class UserCRUD(BaseCRUD):
    async def get_by_username(
        self, session: AsyncSession, username: str
    ) -> Optional[User]:
        statement = select(self.model).where(self.model.username == username)
        result = await session.exec(statement)
        return result.first()


user_crud = UserCRUD(User)
