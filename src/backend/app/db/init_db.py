from sqlmodel.ext.asyncio.session import AsyncSession

from app import crud, schemas
from app.core.config import settings
from app.core.security import get_password_hash

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


async def init_user(db: AsyncSession) -> None:
    # Tables should be created with Alembic migrations
    # But if you don't want to use migrations, create
    # the tables un-commenting the next line
    # Base.metadata.create_all(bind=engine)

    user = await crud.user_crud.get_by_username(db, username=settings.FIRST_SUPERUSER)
    if not user:
        obj = schemas.UserCreate(
            username=settings.FIRST_SUPERUSER,
            email=settings.FIRST_SUPERUSER,
            phone=settings.FIRST_SUPERUSER_PHONE,
            password=get_password_hash(settings.FIRST_SUPERUSER_PASSWORD),
            is_superuser=True,
            avatar_url="",
        )
        await crud.user_crud.create(db, obj=obj)
