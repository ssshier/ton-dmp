from aioredis import Redis, create_redis
from arq.connections import ArqRedis, RedisSettings, create_pool

from app.core.config import settings


async def connect_arq_redis():
    arq_redis = await create_pool(
        RedisSettings(
            host=settings.REDIS_HOST,
            port=settings.REDIS_PORT,
            password=settings.REDIS_PASS,
        )
    )
    Database.arq_redis = arq_redis
    return arq_redis


async def disconnect_arq_redis():
    arq_redis: ArqRedis = Database.arq_redis
    await arq_redis.close()


async def connect_pubsub_redis():
    pubsub_redis = await create_pool(
        RedisSettings(
            host=settings.REDIS_HOST,
            port=settings.REDIS_PORT,
            password=settings.REDIS_PASS,
        )
    )
    Database.pubsub_redis = pubsub_redis
    return pubsub_redis


async def disconnect_pubsub_redis():
    pubsub_redis: Redis = Database.pubsub_redis
    pubsub_redis.close()


class Database:
    arq_redis: ArqRedis = None
    pubsub_redis: Redis = None
