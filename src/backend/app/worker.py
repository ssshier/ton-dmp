import asyncio

from app.tasks.task_consumer import arq_worker

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(arq_worker())
