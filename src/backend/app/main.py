from fastapi import FastAPI, status
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse

from app.api.api_v1.api import api_router
from app.core.config import settings
from app.event import shutdown, startup

app = FastAPI(
    title=settings.PROJECT_NAME, openapi_url=f"{settings.API_V1_STR}/openapi.json"
)

# Set all CORS enabled origins
if settings.BACKEND_CORS_ORIGINS:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@app.exception_handler(Exception)
async def custom_exception_handler(_, exc):
    try:
        status_code = exc.status_code
    except AttributeError:
        status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
    try:
        message = exc.message
    except AttributeError:
        message = f"{exc}"
    content = {"code": status_code, "message": message}
    return JSONResponse(content, status_code=status_code)


app.include_router(api_router, prefix=settings.API_V1_STR)
app.add_event_handler("startup", startup)
app.add_event_handler("shutdown", shutdown)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run("main:app", host="0.0.0.0", port=9000, reload=True)
