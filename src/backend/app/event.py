import signal

from loguru import logger

from app.core.message.pubsub import PubSub
from app.core.message.ws_manager import ws_manager
from app.db.redis import (
    connect_arq_redis,
    connect_pubsub_redis,
    disconnect_arq_redis,
    disconnect_pubsub_redis,
)
from app.db.session import init_table
from app.init_data import init_data


async def startup():
    logger.info("dmp app startup...")
    # await connect_arq_redis()
    logger.info("connect pubsub redis...")
    await connect_pubsub_redis()

    logger.info("init table and data...")
    await init_table()
    await init_data()

    logger.info("start pubsub...")
    pubsub = PubSub()
    await pubsub.open_ws_sender()
    await pubsub.subscribe()

    logger.info("start signal watch...")
    signal.signal(signal.SIGTERM, kill)  # type: ignore


async def shutdown():
    logger.info("dmp app shutdown...")
    # await disconnect_arq_redis()
    logger.info("disconnect pubsub redis...")
    await disconnect_pubsub_redis()

    logger.info("stop pubsub...")
    pubsub = PubSub()
    await pubsub.stop()


def kill(*args, **kwargs):
    logger.info("kill...")
    import asyncio

    asyncio.ensure_future(ws_manager.disconnect_all())
