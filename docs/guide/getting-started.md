# Getting Started

## Develop

### Backend

```shell
export PYTHONPATH=$PYTHONPATH:`PWD`/src/backend
cd src/backend
python app/main.py
```

### Worker

```shell
export PYTHONPATH=$PYTHONPATH:`PWD`/src/backend
cd src/backend
python app/worker.py
```

### Frontend

```shell
cd src/frontend
pnpm i
# dev
pnpm dev
# build
pnpm build
```
