# What is TON-DMP

FastAPI & Vue3 Powered Project Kit

## Technology Stack

| Backend       | Worker        | Frontend      |
| ------------- |:-------------:|:-------------:|
| FastAPI       | Arq           | Vue3          |
| SqlAlchemy    | aioredis      | NavieUI       |
| SQLModel      | ...           | Vite          |
| SLY           | ...           | ...           |
| ...           | ...           | ...           |
