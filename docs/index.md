---
layout: home

title: TON-DMP
titleTemplate: FastAPI & Vue3 Powered Project Kit

hero:
  name: TON-DMP
  text: FastAPI & Vue3 Powered Project Kit
  tagline: Simple, Powerful, and Performant.
  actions:
    - theme: brand
      text: Get Started
      link: /guide/getting-started
    - theme: alt
      text: View on Gitee
      link: https://gitee.com/ssshier/ton-dmp

features:
  - title: "Init project"
    details: Init backend, frontend, worker.
---
