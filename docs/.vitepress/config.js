import { version } from '../../package.json'


export default {
  title: "TON-DMP",
  description: "FastAPI & Vue3 Powered Project Kit",
  lastUpdated: true,
  cleanUrls: "without-subfolders",

  themeConfig: {
    nav: nav(),

    sidebar: {
      "/guide/": sidebarGuide(),
      "/config/": sidebarConfig(),
    },

    editLink: {
      pattern: 'https://gitee.com/ssshier/ton-dmp/edit/master/docs/:path',
      text: 'Edit this page on Gitee'
    },

    socialLinks: [
      {
        icon: {
          svg: '<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><path fill="#c71d23" d="M11.984 0A12 12 0 0 0 0 12a12 12 0 0 0 12 12a12 12 0 0 0 12-12A12 12 0 0 0 12 0a12 12 0 0 0-.016 0zm6.09 5.333c.328 0 .593.266.592.593v1.482a.594.594 0 0 1-.593.592H9.777c-.982 0-1.778.796-1.778 1.778v5.63c0 .327.266.592.593.592h5.63c.982 0 1.778-.796 1.778-1.778v-.296a.593.593 0 0 0-.592-.593h-4.15a.592.592 0 0 1-.592-.592v-1.482a.593.593 0 0 1 .593-.592h6.815c.327 0 .593.265.593.592v3.408a4 4 0 0 1-4 4H5.926a.593.593 0 0 1-.593-.593V9.778a4.444 4.444 0 0 1 4.445-4.444h8.296Z"/></svg>'
        },
        link: "https://github.com/ssshier",
      },
    ],

    footer: {
      message: "Released under the MulanPSL2 License.",
      copyright: "Copyright © 2022-present ssshier",
    }
  },
};

function nav() {
  return [
    {
      text: "Guide",
      link: "/guide/what-is-ton-dmp",
      activeMatch: "/guide/",
    },
    {
      text: "Configs",
      link: "/config/introduction",
      activeMatch: "/config/",
    },
    {
      text: version,
      items: [
        {
          text: "link",
          link: "https://gitee.com/ssshier/ton-dmp",
        }
      ],
    },
  ];
}

function sidebarGuide() {
  return [
    {
      text: "Introduction",
      collapsible: true,
      items: [
        {
          text: "What is TON-DMP?",
          link: "/guide/what-is-ton-dmp",
        },
        {
          text: "Getting Started",
          link: "/guide/getting-started",
        },
        {
          text: "Configuration",
          link: "/guide/configuration",
        },
        {
          text: "Deploying",
          link: "/guide/deploying",
        },
      ],
    },
  ];
}

function sidebarConfig() {
  return [
    {
      text: "Config",
      items: [
        {
          text: "Introduction",
          link: "/config/introduction",
        }
      ],
    },
  ];
}
